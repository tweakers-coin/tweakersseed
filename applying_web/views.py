from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse
from django.views import generic

from .forms import ApplyForm


def index(request):
    context = {}
    return render(request, 'TweakersSeed/index.html', context)


class ApplyView(generic.FormView):
    template_name = 'TweakersSeed/apply.html'

    form_class = ApplyForm

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, '신청이 완료되었습니다.')

        return reverse('index')
