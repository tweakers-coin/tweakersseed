from django.contrib import admin

from .models import ApplyModel


# Register your models here.

class ApplyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone')
    model = ApplyModel


admin.site.register(ApplyModel, ApplyAdmin)
