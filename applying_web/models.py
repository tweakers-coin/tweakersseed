from django.db import models

# Create your models here.
from phonenumber_field.modelfields import PhoneNumberField


class ApplyModel(models.Model):
    class Meta:
        verbose_name = '지원'
        verbose_name_plural = "지원서"
    name = models.CharField('이름',max_length=20, help_text='eg) 신윤수')
    major = models.CharField('학과', max_length=255, help_text='벤처중소기업학과')
    sid = models.CharField('학번', max_length=255, help_text='2010xxxx')

    email = models.EmailField('이메일', help_text='xxxxx@naver.com')
    phone = PhoneNumberField('휴대폰', help_text='01012341234')
    is_synergy = models.BooleanField('시너지 여부', default=False)
    synergy_ki = models.IntegerField('몇기?', null=True, blank=True)

    want_service = models.TextField('만들고 싶은 서비스?')

    self_intro = models.TextField('자기소개 좀 부탁해요!')
    
    is_accept = models.BooleanField('통과 여부', default=False)
