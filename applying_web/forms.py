from django import forms
from django.forms.utils import ErrorList

from .models import ApplyModel

class ApplyForm(forms.ModelForm):
    class Meta:
        model = ApplyModel
        exclude = ('is_accept',)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)


        for field in self.fields:
            # print(self.fields[field].widget.attrs['placeholder'])
            # print(self.fields[field].widget.help_text)
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].help_text
            # print(self.fields[field].help_text)
            # print(type(field))
            # print(field)

    def save(self, commit=True):
        objects = super().save(commit)
        return objects




