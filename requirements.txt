Babel==2.5.3
Django==2.0.4
django-phonenumber-field==2.0.0
django-widget-tweaks==1.4.2
phonenumberslite==8.9.3
pytz==2018.4
